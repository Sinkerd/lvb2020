import Vue from "vue";
import Router from "vue-router";
import Index from "./Index";
import store from '../stores';
import { i18n } from '@/plugins/lang/i18n'
import code from "@/helper/fuck.js";
Vue.use(Router);
var routers = [];
routers = routers.concat(Index);
const routes = routers;

const router = new Router({
  mode: "history",
  base: "/lvb/",
  routes
});

router.beforeEach((to, from, next) => {  
  let thor = {
    user: null
  }; 
  setLang();
  if (localStorage.getItem("thor")) {
    thor = JSON.parse(code.from((localStorage.getItem("thor"))));
  }  
  if (to.matched.some(record => record.meta.requiresAuth)) {    
    if (thor.user == null) {   
      console.log('aa' + thor.user);   
      next({
        path: "/signin"
      });
    } else {  
      console.log(thor.user);
      next();
    }
  } else {   
    console.log('aa' + thor.user);    
    next();
  }
});

const setLang = function(){  
  if (store.state.language.language && store.state.language.language !== i18n.locale) {
    i18n.locale = store.state.language.language;
  } else if (!store.state.language.language) {
    store.dispatch('language/setLanguage', navigator.languages)
      .then(() => {
        i18n.locale = store.state.language.language;
      });
  }
  else {
    i18n.locale = 'la';
  } 
}

export default router;