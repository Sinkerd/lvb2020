const Login = () => import('@/views/auth/SignIn.vue');
const Main = () => import('@/layout/MainLayout.vue')
const route = [
    {
        path: '/',
        name: 'Main',
        component: Main,
        meta: { requiresAuth: true }, // check authication krn ja ja khao nar luk thar br tg karn check krn man sai false
        children : [
            {
                path: '/setting',
                name: 'Setting',
                component: () => import( /* webpackChunkName: "user" */ '@/views/setting/setting.vue'),
            },
            {
                path: '/service',
                name: 'Service',
                component: () => import( /* webpackChunkName: "user" */ '@/views/service/service.vue'),
                // beforeEnter: (to, from,next) => {
                //     console.log(from);
                //     next();                    
                // }
            },
            {
                path: '/user',
                name: 'User',
                component: () => import( /* webpackChunkName: "user" */ '@/views/user/user.vue'),
            },
            {
                path: '/form',
                name: 'form',
                component: () => import( /* webpackChunkName: "user" */ '@/views/Form/Form.vue'),
            },
            {
                path: '/menutax',
                name: 'menuTax',
                component: () => import( /* webpackChunkName: "user" */ '@/views/Form/menutax.vue'),
            },
             {
                path: '/OnlinePayment',
                name: 'onlinepayment',
                component: () => import( /* webpackChunkName: "user" */ '@/views/Form/OnlinePayment.vue'),
            },
            {
                path: '/NewOnlinePayment',
                name: 'NewEasyTax',
                component: () => import( /* webpackChunkName: "user" */ '@/views/Form/NewOnlinepayment.vue'),
            }
          
        ]
    },
  
    {
        path: '/signin',
        name: 'Login',
        component: Login,
    }
]

export default route