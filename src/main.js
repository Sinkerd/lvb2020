import Vue from 'vue'
import App from './App.vue'
import router from './router/router'
import "vue-material-design-icons/styles.css"
import 'animate.css/animate.min.css'
import material from "./plugins/icons/meterial"
import moment from 'vue-moment'
import store from './stores/index.js'
import { i18n } from './plugins/lang/i18n.js'
import MsgBox from './helper/modal.js';


//veu axios
import axios from 'axios'
import VueAxios from 'vue-axios'

//currency input
import VueCurrencyInput from 'vue-currency-input'

const pluginOptions = {
  /* see config reference */
  globalOptions: { currency: 'LAK' }
};
Vue.use(VueCurrencyInput, pluginOptions)


//add new
import BootstrapVue from 'bootstrap-vue/dist/bootstrap-vue.esm';

Vue.use(MsgBox);
Vue.use(moment)

import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)

Vue.use(material)
Vue.config.productionTip = false

//add new
Vue.use(BootstrapVue);

//new vue axois
Vue.use(VueAxios, axios)



new Vue({
  router,
  store,
  i18n,
  render: h => h(App),
}).$mount('#app')