import Vue from 'vue';
import Vuex from 'vuex';

//Modules
import language from './modules/language';
import authentication from './modules/Authentication';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    language,
    authentication
  }
});

export default store;
