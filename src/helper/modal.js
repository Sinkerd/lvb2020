import Template from '@/components/modal/modalConfirm.vue';
import modalSuccess from '@/components/modal/modalSuccess.vue';
let globalOptions = {
    show: false,
    closable: true,
    title: {
        content: '',
        cssClass: '',
        style: {}
    },
    message: {
        content: '',
        cssClass: '',
        style: {}
    },
    buttons: [],
    closeBtn: {
        cssClass: '',
        style: {}
    },
    messageBox: {
        cssClass: '',
        style: {}
    },
    msgFooter: {
        cssClass: '',
        style: {}
    }
};

let instance;

const Message = function(config = {}) {
    var s = document.getElementsByClassName('legend-modal-addproduct');
    if(s.length == 0){
    let Tpl = this.extend(Template);
    instance = new Tpl();
    config = {
        ...globalOptions,
        ...config
    };
    for(let key in config) {
        if(config.hasOwnProperty(key)) {
            instance.$data[key] = config[key];
        }
    }
    instance.$data.show = true;
    document.body.style.overflow = 'hidden';
    document.body.appendChild(instance.$mount().$el);
    }
}

const Confirm = function(title,message){
    return new Promise((resolve) => {
    let confirmConfig = {
        type: 'info',
        title: {
            content: title,
        },
        message: {
            content: message
        },
        buttons: [{
            label: 'ຕົກລົງ',
            cssClass : 'btn btn-save-modal-addproduct',
            style: {
                marginRight: '4px'
            },
            action: function(){
                Dismiss();
                resolve(true)
                //callback && callback(true);
            }
        }, {
            label: 'ຍົກເລີກ',
            cssClass : 'btn btn-cancel-addproduct',
            style: {
               
            },
            action: function(){
                Dismiss();
            }
        }],
        msgFooter: {
            style: {
                padding: '0'
            }
        }
    };
    
    Message.call(this, {
        ...globalOptions,
        ...confirmConfig
    });
    })
}


const MessageSuccess = function(config = {}) {
    let Tpl = this.extend(modalSuccess);
    instance = new Tpl();
    config = {
        ...globalOptions,
        ...config
    };
    for(let key in config) {
        if(config.hasOwnProperty(key)) {
            instance.$data[key] = config[key];
        }
    }
    instance.$data.show = true;
    document.body.style.overflow = 'hidden';
    document.body.appendChild(instance.$mount().$el);
}

const Success = function(title,message){
    return new Promise((resolve) => {
    let confirmConfig = {
        type: 'info',
        title: {
            content: title,
        },
        message: {
            content: message
        },
        buttons: [{
            label: 'ຕົກລົງ',
            cssClass : 'btn btn-save-modal-addproduct',
            style: {
               
            },
            action: function(){
                Dismiss();
                resolve(true)
                //callback && callback(true);
            }
        }],
        msgFooter: {
            style: {
                padding: '0'
            }
        }
    };
    
    MessageSuccess.call(this, {
        ...globalOptions,
        ...confirmConfig
    });
    })
}

const Warning = function(title,message){
    let confirmConfig = {
        type: 'info',
        title: {
            content: title,
        },
        message: {
            content: message
        },
        buttons: [{
            label: 'ຕົກລົງ',
            cssClass : 'btn btn-save-modal-addproduct',
            style: {
               
            },
            action: function(){
                Dismiss();
               
            }
        }],
        msgFooter: {
            style: {
                padding: '0'
            }
        }
    };
    
    Message.call(this, {
        ...globalOptions,
        ...confirmConfig
    });
}


const Dismiss = () => {
    instance.$data.show = false;
    document.body.style.overflow = 'auto';
}

export default {
    install(Vue) {
        Vue.prototype.$Message = Message.bind(Vue);
        Vue.prototype.$Message.confirm = Confirm.bind(Vue);
        Vue.prototype.$Message.success = Success.bind(Vue);
        Vue.prototype.$Message.warning = Warning.bind(Vue);
        Vue.prototype.$Message['dismiss'] = Dismiss;
    }
}