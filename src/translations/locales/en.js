export default {
  language : {
    'en' : 'English',
    'la' : 'Laos',
    'vi' : 'Vietnam'
  },
  Language : 'Language',
  LaovietBank : 'Lao-Viet Bank',
  Logout : 'Logout',
  Areyousuretologout : 'Are you sure to logout?',
  Setting : 'Setting',
  Service : 'Service',
    //Mr.sin add languages
  Payment:'Are you sure to payment?',
  Accept:'Accept Payment',
  Cancel:'Cancel',

  Del:'Del',
  AddList:'Add List',
  No:'No',
  Nameoflist:'Name of List',
  Accno:'Account No',
  Amount:'Amount',
  Manage:'Manage',
  Total:'Total',
  pleaseaddname:'Please enter name of list',
  pleaseenteraccno:'Please enter account no',
  offlinePayment:'Offline payment of easy tax',
  paymentTax:'Easy tax payment',
  infobill:'Info of Bill'



};
