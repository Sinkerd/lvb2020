export default {
  language : {
    'en' : 'ພາສາອັງກິດ',
    'la' : 'ພາສາລາວ',
    'vi' : 'ພາສາຫວຽດ'
  },
  Language : 'ພາສາ',
  LaovietBank : 'ທະນາຄານຮ່ວມທຸລະກິດລາວ-ຫວຽດ',
  Logout : 'ອອກຈາກລະບົບ',
  Areyousuretologout : 'ທ່ານຕ້ອງານອອກຈາກລະບົບແທ້ບໍ່?',
  Setting : 'ຕັ້ງຄ່າ',
  Service : 'ບໍລິການ',
  //Mr.sin add languages
  Payment:'ທ່ານຕ້ອງການ ການຊຳລະແທ້ບໍ່?',
  Accept:'ຢືນຢັນການຊຳລະ',
  Cancel:'ຍົກເລີກ',

  Del:'ລົບ',
  AddList:'ເພີ່ມລາຍການ',
  No:'ລຳດັບ',
  Nameoflist:'ຊື່ລາຍການ',
  Accno:'ເລກບັນຊີ',
  Amount:'ຈຳນວນເງິນ',
  Manage:'ການຈັດການ',
  Total:'ລວມ',
  pleaseaddname:'ກະລຸນາປ້ອນຊື່ລາຍການ',
  pleaseenteraccno:'ກະລຸນາປ້ອນເລກບັນຊີ',
  offlinePayment:'ລະບົບການຊຳລະຄ່າພາສີອາກອນແບບ Offline',
  paymentTax:'ການຊຳລະຄ່າພາສີອາກອນ',
  infobill:'ຂໍ້ມູນລາຍການຂອງໃບບິນ',
  bill:'ສະມຸດໃບບິນ',
  info:'ເນື້ອໃນການຊຳລະ',
  onlinepayment:'ລະບົບການຊຳລະຄ່າພາສີອາກອນແບບ Online',
  bottom:'data',
  detial:'ເນື້ອໃນການຊຳລະ',
  success:'ສຳເລັດການຊຳລະ',







};
