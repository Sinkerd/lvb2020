export default {
  language : {
    'en' : 'English',
    'la' : 'Laos',
    'vi' : 'Vietnam'
  },
  Language : 'Language',
  LaovietBank : 'Lao-Viet Bank',
  Logout : 'Logout',
  Areyousuretologout : 'Are you sure to logout?',
  Setting : 'Setting',
  Service : 'Service',
};
