import en from './locales/en';
import la from './locales/la';

export default {
  en,
  la,
};
